import java.rmi.registry.*;
import java.rmi.server.UnicastRemoteObject;

public class CalculatorServer {
    public static void main(String args[]) {
        try {
            // Create the RMI Registry
            Registry registry = LocateRegistry.createRegistry(1099);

            // Create the Remote Object
            Calculator c = (Calculator) 
                UnicastRemoteObject.exportObject(new CalculatorImpl(),0);

            // Register the object with the RMI Registry
            registry.bind("CalculatorService", c);
            
            System.out.println("STOPPING SERVER SHUTDOWN");
            while (true) {Thread.sleep(1000); }
        } catch (Exception e) {
            System.out.println("Trouble: " + e);
        }
    }
}
