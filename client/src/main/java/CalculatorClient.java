import java.rmi.registry.*;

public class CalculatorClient {
    public static void main(String[] args) {
        String host = "localhost";
        if (args.length > 0) {
            host = args[0];
        }
        try {
            // Get a reference to the RMI Registry
            Registry registry = LocateRegistry.getRegistry(host, 1099);

            // Find the distributed object (stub created here)
            System.out.println("Names:");
            for(String name : registry.list()) {
                System.out.println(name);
            }
            Calculator c = (Calculator) registry.lookup("CalculatorService");

            // Do stuff!!!!
            System.out.println("Calls:");
            System.out.println(c.sub(4, 3));
            System.out.println(c.add(4, 5));
            System.out.println(c.mul(3, 6));
            System.out.println(c.div(9, 3));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
