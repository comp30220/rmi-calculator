REM my project build script
cd core
call mvn install
cd ..\server
call mvn package
cd ..\client
call mvn package
cd ..
call docker-compose up
pause